//
//  TodayViewController.h
//  Konotor Deals
//
//  Created by Sukanya D on 20/02/15.
//  Copyright (c) 2015 Sukanya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodayViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *dealText;
@property (strong, nonatomic) IBOutlet UIImageView *dealLogo;
@property (strong, nonatomic) IBOutlet UIPageControl *imageSlider;

@end
