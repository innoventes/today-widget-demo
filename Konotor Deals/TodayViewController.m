//
//  TodayViewController.m
//  Konotor Deals
//
//  Created by Sukanya D on 20/02/15.
//  Copyright (c) 2015 Sukanya. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>

@interface TodayViewController () <NCWidgetProviding>

@end

@implementation TodayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self updateWidget];
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollText{
    NSString *text = self.dealText.text;
    if ([text isEqualToString:@"50% off on Watches"]) {
      self.dealText.text = @"30% off on Bags";
    }else if([text isEqualToString:@"30% off on Bags"]){
        self.dealText.text = @"25% off on iPhone 5s";
    }else {
        self.dealText.text = @"50% off on Watches";
    }
}

- (void)scrollImage {
    NSInteger i = self.imageSlider.currentPage;
    if(i == 0){
        [self.dealLogo setImage:[UIImage imageNamed: @"deal2.png"]];
        self.imageSlider.currentPage = 1;
    }else if( i == 1){
        [self.dealLogo setImage:[UIImage imageNamed: @"deal3.png"]];
        self.imageSlider.currentPage = 2;
    }else if(i == 2){
        [self.dealLogo setImage:[UIImage imageNamed: @"deal1.png"]];
        self.imageSlider.currentPage = 0;
    }
    [self scrollText];
}


- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData

    completionHandler(NCUpdateResultNewData);
}

- (UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)defaultMarginInsets
{
    return UIEdgeInsetsZero;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(userDefaultsDidChange:)
                                                     name:NSUserDefaultsDidChangeNotification
                                                   object:nil];
    }
    return self;
}

- (void)userDefaultsDidChange:(NSNotification *)notification {
    [self updateWidget];
}

- (void)updateWidget {
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.innoventes.konotor"];
    NSInteger value = [shared integerForKey:@"DisplayDeal"];
    NSLog(@"%li",(long)value);
    if (value == 1) {
        [[NSTimer scheduledTimerWithTimeInterval:3
                                          target:self
                                        selector:@selector(scrollText)
                                        userInfo:Nil
                                         repeats:YES] fire];
        self.dealLogo.hidden = TRUE;
        self.preferredContentSize = CGSizeMake(0, 50);
        
    }else{
        self.preferredContentSize = CGSizeMake(0, 200);
        
        [[NSTimer scheduledTimerWithTimeInterval:3
                                          target:self
                                        selector:@selector(scrollImage)
                                        userInfo:Nil
                                         repeats:YES] fire];
    }

}

@end
