//
//  AppDelegate.h
//  konotor
//
//  Created by Sukanya D on 20/02/15.
//  Copyright (c) 2015 Sukanya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

