//
//  main.m
//  konotor
//
//  Created by Sukanya D on 20/02/15.
//  Copyright (c) 2015 Sukanya. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
