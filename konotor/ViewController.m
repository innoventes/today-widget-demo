//
//  ViewController.m
//  konotor
//
//  Created by Sukanya D on 20/02/15.
//  Copyright (c) 2015 Sukanya. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.innoventes.konotor"];
    [sharedDefaults setInteger:0 forKey:@"DisplayDeal"];
    [sharedDefaults synchronize];
    NSInteger value = [sharedDefaults integerForKey:@"DisplayDeal"];
    self.parameter.text = [NSString stringWithFormat:@"Parameter Passed %li",(long)value];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)updateDefaults:(id)sender {
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.innoventes.konotor"];
    NSString *text = self.para.text;
    int entered = [text intValue];
    [sharedDefaults setInteger:entered forKey:@"DisplayDeal"];
    [sharedDefaults synchronize];
    NSInteger value = [sharedDefaults integerForKey:@"DisplayDeal"];
    self.parameter.text = [NSString stringWithFormat:@"Parameter Passed %li",(long)value];
}

@end
