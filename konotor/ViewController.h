//
//  ViewController.h
//  konotor
//
//  Created by Sukanya D on 20/02/15.
//  Copyright (c) 2015 Sukanya. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *parameter;
@property (strong, nonatomic) IBOutlet UITextField *para;
@property (strong, nonatomic) IBOutlet UIButton *change;


@end

